---
title: "Showcase Footer"
anchor: showcase_footer
---

---

{{< hint type=info title="Want to get your game submission featured here?" >}}

Don't forget to share your progress on the [community discord](https://discord.gg/mBGd9hahZv)!

I'll periodically update this section with community submissions. Feel free to message me if you want yours to be included.
You can also open a pull request yourself to add your game, or someone else's.
{{< /hint >}}