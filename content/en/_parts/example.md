---
title: "Example Game"
anchor: example
---

## A Game! - some basic shooter

Here is some fun information about the game. Release date, random trivia, etc.

### Difficulty
Difficulty will be split into two parts: Complexity and Scope.  
|                  |                                              |                       |
| :---             | :---                                         | :---                  |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} | *Out of 5 stars total |
| Scope            | {{< icon "star" >}} {{< icon "star" >}}      | *Out of 5 stars total |

{{< hint type=tip title="Complexity vs Scope">}}
Complexity is the technical level of the game. More advanced topics will have a higher score than less advanced topics.

Scope is the amount of stuff you will need to make. Art, animation, music, etc. More stuff will take longer to make.

This is a best-guess estimate. More stars means that you will need to dedicate more time to learning or making content.
{{< /hint >}}

### Definition of Done
Here is a list of game elements that you will need to complete. Once you do everything on the list, you can say that you made this game! 
* Player
* Enemies
* Bullets and collision
* etc.

Stretch goal: This isn't strictly necessary, but you should add one or more of these if you have time.
* Particle effects
* Pause menu
* etc.

### Showcase
You are not the first person to try to make this game! Here are some success stories and helpful resources related to this game.
