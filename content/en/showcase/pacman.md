---
title: "Pac_Man Showcase"
anchor: "pacman_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Erip made a [web playable game](https://games.petzel.io/pacman/)

* Greg made a [web playable game](https://an-unique-name.itch.io/pac-clone)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/pac-man-5-in-the-20-games-challenge)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/realm-of-pac)

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/zep-man) with [source code](https://github.com/AlixBarreaux/zep-man)

* Levi Was Rigged From The Start made a [web playable game](https://boofgall-games.itch.io/pacman)

* Eka made a [downloadable game](https://yakdoggames.itch.io/astro-man) with gameplay footage:
  {{< youtube 3Mb4MmUhG2c >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/pac-man):
  {{< youtube 221sPOp_514 >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
