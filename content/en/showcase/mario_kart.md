---
title: "Mario Kart Showcase"
anchor: "mario_kart_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/mario-kart):
  {{< youtube LxuUl-YQbMA >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
