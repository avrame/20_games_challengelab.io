---
title: "Conway's Game of Life Showcase"
anchor: "life_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Andrew made a [web playable game](https://frenata.itch.io/game-of-life) with [source code](https://gitlab.com/frenata-20-game-challenge/game-of-life)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/conways-game-of-life)

* StalOlympus made a [web playable game](https://stalolympus.itch.io/conways-game-of-life)

{{< include file="_parts/showcase_footer.md" type=page >}}
