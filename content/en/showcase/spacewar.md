---
title: "Spacewar! Showcase"
anchor: "spacewar_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* StalOlympus made a [web playable game](https://stalolympus.itch.io/spacewar)

* Crowilly made a [web playable game](https://crowilly.itch.io/spacewar)

{{< include file="_parts/showcase_footer.md" type=page >}}
