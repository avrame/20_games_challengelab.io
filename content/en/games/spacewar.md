---
title: "Spacewar!"
anchor: "spacewar"
weight: 41
---

Spacewar is one of the earliest known video games. It appeared on the PDP-1 minicomputer at MIT in 1962. Spacewar featured two space ships orbiting a star. Players had to maneuver around the star and shoot their opponent before being shot. The game inspired Atari to make "Computer Space" - the first commercial arcade game.

The game was initially controlled via switches on the computer, but a student at the university designed and built a dedicated control box to prevent excited players from breaking the expensive research computer. The game was quickly banned from the computer lab during working hours due to its addictive nature (and the tendency of students to not get any work done while the game was being played!)

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} |
| Scope            | {{< icon "star" >}} | 

### Goal:
* Create two different ships. One player will control each ship. Ships can rotate and thrust. Thrusting will accelerate the ship "forward" in the direction that it is facing.
* Add the ability for ships to fire torpedoes. The player ship will fire in the direction that it is facing. Torpedoes will disappear after a short while.
* Enable collisions. If two players collide, both lose. If a torpedo hits a player, the other player wins. If two torpedoes collide, both are destroyed.
* Enable screen wrapping. (Objects leaving the top of the screen should enter the bottom, for example)
* Add a star at the center of the screen. Apply gravity (everything will slowly fall towards the star)
* Add sound effects and particles!

### Stretch goal:
* The original Spacewar! was multiplayer only, but feel free to add an AI opponent to face off against!
* Alternatively, this might be a good time to try to make an online multiplayer game. (if that's a skill that you intend to learn)

{{< expand "Showcase" >}} {{< include file="showcase/spacewar.md" type=page >}} {{< /expand >}}
