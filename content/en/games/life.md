---
title: "Conway's Game of Life"
anchor: "life"
weight: 52
---

Conway's Game of Life is not technically a video game; it is a cellular automata. Referred to as a "zero player game," it requires no input beyond the initial state. British mathematician John Conway devised the rules for the simulation in 1970, doing the majority of his experiments with pen and paper.

The Game of Life has become a staple of recreational computing and recreational mathematics. The game helped launch the field of cellular automata. The Game of Life is impressive because the rules are so simple, and yet the outcome can be incredibly complex. The Game of Life is Turing-complete, which means that you could theoretically use it as a language to make your next game in! (though that would not be advised...)

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}}                          | 

### Goal:
* Create the "Universe" - a square grid with cells that can be "alive" or "dead," represented by two different colors.  
The universe is infinite in theory, but you might want to pick a grid size that can fit within a computer's memory 🙂
* The simulation will occur in steps, or "generations." Make the appropriate data structures to track both the current and future generation.
* Allow the player to toggle any of the cells between alive and dead.
* Allow the player to start, stop, and reset the simulation. You might also want to make the simulation speed variable.
* During the simulation, each cell will follow a simple set of rules:
  * Any live cell with fewer than two live neighbors dies.
  * Any live cell with two or three neighbors lives on in the next generation.
  * Any live cell with more than three live neighbors dies.
  * Any dead cell with exactly three live neighbors becomes alive in the next generation.

### Stretch goal:
* Feel free to mess with the rules to see what happens. Alternatively, you can try to implement other [cellular automata](https://www.youtube.com/watch?v=kzwT3wQWAHE) if you're up for a bigger challenge.
* Get lost in the [game of life wiki](https://conwaylife.com/wiki/) for hours, and implement your own oscillators, spaceships, gliders, guns, and puffers. Watch the little critters crawl around your game all on their own.

{{< expand "Showcase" >}} {{< include file="showcase/life.md" type=page >}} {{< /expand >}}
