import { sortTables, STATE, ROTATED_CLASS, HIDDEN_CLASS } from "./utility.mjs";

(function () {
  sortTables((clickedHeader, headers) => {
    if (headers) {
      headers.forEach((header) => {
        const arrow = header.querySelector(".icon-arrow");
        if (header === clickedHeader) {
          arrow.classList.remove(HIDDEN_CLASS);
          if (header.dataset.state === STATE.DESCENDING) {
            arrow.classList.add(ROTATED_CLASS);
          } else {
            arrow.classList.remove(ROTATED_CLASS);
          }
        } else {
          arrow.classList.add(HIDDEN_CLASS);
        }
      });
    }
  });
})();
